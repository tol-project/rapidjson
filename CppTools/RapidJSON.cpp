/* TolRInside.cpp: C++ API to invoke R from TOL

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

//Starts local namebock scope

#include <sstream>
#include "rapidjson/reader.h"
#include "rapidjson/error/en.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/document.h"

using namespace rapidjson;

#define LOCAL_NAMEBLOCK _local_namebtntLock_
#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_bsetgra.h>
#include <tol/tol_btxtgra.h>
#include <tol/tol_bdatgra.h>

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& LOCAL_NAMEBLOCK = _local_unb_->Contens();

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function
DynAPI void* GetDynLibNameBlockRapidJSON()
{
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = _local_unb_->Contens();
  return(copy);
}

void Array2Set(const Value &array, BSet& result);
void Object2Set(const Value &object, BSet& result);

BSyntaxObject *Value2TOL(const Value &v)
{
    BSyntaxObject *obj;
    if (v.IsString())
      {
      obj = new BContensText(v.GetString());
      }
    else if (v.IsNumber())
      {
      obj = new BContensDat("", v.GetDouble());
      }
    else if (v.IsObject())
      {
      BUserSet *tmp = new BContensSet;
      Object2Set(v, tmp->Contens());
      obj = tmp;
      }
    else if (v.IsArray())
      {
      BUserSet *tmp = new BContensSet;
      Array2Set(v, tmp->Contens());
      obj = tmp;
      }
    else if(v.IsBool())
      {      
      obj = new BContensDat("", v.GetBool());
      }
    else
      {
      obj = new BContensDat("", BDat::Unknown());
      }
    return obj;
}

void Array2Set(const Value &array, BSet& result)
{
  // if document is array
  for (Value::ConstValueIterator itr = array.Begin(); itr != array.End(); ++itr)
    {
    result.AddElement(Value2TOL(*itr));
    }
}

void Object2Set(const Value &doc, BSet& result)
{
  // if document is array
  for (Value::ConstMemberIterator itr = doc.MemberBegin();
      itr != doc.MemberEnd(); ++itr)
    {
    BSyntaxObject *obj = Value2TOL(itr->value);
    obj->PutName(itr->name.GetString());
    result.AddElement(obj);
    }
}

void Document2Set(const Document &doc, BSet& result)
{
  if(doc.IsObject())
    {
    Object2Set(doc, result);
    }
  else if(doc.IsArray())
    {
    Array2Set(doc, result);
    }
  else
    {
    Error("Only json container can be parsed into Set");
    }
  /*
  bool isObject = doc.IsObject();
  for (Value::ConstMemberIterator itr = doc.MemberBegin();
      itr != doc.MemberEnd(); ++itr)
    {
    BSyntaxObject *obj = Value2TOL(itr->value);
    if(isObject)
      {
      obj->PutName(itr->name.GetString());
      }
    result.AddElement(obj);
    }
*/
}

//---------------------------------------------------------------------------
DeclareContensClass(BSet, BSetTemporary, BSetJSON2Set);
DefMethod(1, BSetJSON2Set, "JSON2Set", 1, 1, "Text", "(Text jsonText)",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BSetJSON2Set::CalcContens()
{
  BText &txtJSON = Text(Arg(1));

  Document dom;
  ParseResult ok = dom.Parse(txtJSON.String());
  if(ok)
    {
    Document2Set(dom, contens_);
    }
  else
    {
    char buffer[2048];
    std::stringstream ss;
    ss << "JSON parse error: " << GetParseError_En(ok.Code()) << " ("
       << ok.Offset() << ")";
    Error(ss.str().c_str());
    }
}

void Set2JSON(BSet & set, Writer<StringBuffer>& writer)
{
  bool isObject;
  if (set.Card() && set[1]->HasName())
    {
    writer.StartObject();
    isObject = true;
    }
  else
    {
    writer.StartArray();
    isObject = false;
    }
  for( int i = 1; i <= set.Card(); i++ )
    {
    BSyntaxObject* obj = set[i];
    if(isObject)
      {
      writer.Key(obj->Name().String());
      }
    if (obj->Grammar() == GraSet())
      {
      BSet& child = Set(obj);
      Set2JSON(child, writer);
      }
    else if (obj->Grammar() == GraReal())
      {
      BDat& x = Dat(obj);
      if(x.IsKnown())
	{
	double _x = x.GetValue();
	
	if (fmod(_x, 1) == 0.0)
	  writer.Int64(static_cast<int64_t>(_x));
	else 
	  writer.Double(_x);
        
	/*
	if(floor(_x)==_x)
	  writer.Int(static_cast<int>(_x));
	else
	  writer.Double(_x);
        */
	}
      else
	{
	writer.Null();
	}
      }
    else if (obj->Grammar() == GraText())
      {
      BText &txt = Text(obj);
      writer.String(txt.String());
      }
    else
      {
      std::string x = "<internal:";
      x += obj->Grammar()->Name().String();
      x += ">";
      writer.String(x.c_str());
      }
    }
  if (isObject)
    {
    writer.EndObject();
    }
  else
    {
    writer.EndArray();
    }
}

//---------------------------------------------------------------------------
DeclareContensClass(BText, BTxtTemporary, BTxtSet2JSON);
DefMethod(1, BTxtSet2JSON, "Set2JSON", 1, 1, "Set", "(Set doc)",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BTxtSet2JSON::CalcContens()
{

  StringBuffer s;
  Writer<StringBuffer> writer(s);
  BSet& set = Set(Arg(1));
  
  Set2JSON(set, writer);
  contens_ = s.GetString();
}
